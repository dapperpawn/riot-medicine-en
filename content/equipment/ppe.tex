\chapter{Personal Protective Equipment}
\label{ch:ppe}

% TODO source this but I'm too lazy now
\epigraph{
The history of progress is written in the blood of men and women who have dared to espouse an unpopular cause, as, for instance, the black man's right to his body, or woman's right to her soul.
}{Emma Goldman}

\index{personal protective equipment|(}

\noindent
In order to be able to effectively render care to others, you must keep your self sufficiently safe to be able to reach them and avoid injury until the end of the action.
This chapter will discuss non-medical \acrlong{PPE} (\acrshort{PPE}).
This equipment will help keep you safe from flying bottles, chemical agents, and other dangers you may encounter while in the field.

It may be illegal\index{legality} to wear PPE or even bring it to demonstrations.
Some regions classify PPE as weapons or ``passive weapons'' (items designed to make the wearer resistant to police violence).
You will need to decide what (if any) items you will bring or wear to actions by considering both the risk of bodily harm and police repression tactics.
Discussion of what equipment you may want to bring or wear are discussed in \fullref{ch:packing_your_bag} and \fullref{ch:general_tactics} respectively.

When if comes to protection against chemical agents, most medics are only concerned with protection against \glspl{riot control agent} (\acrshort{RCA}s).
Protection against biological, chemical, and radiological weapons is outside the scope of this book.

\section*{Helmets}
\index{helmets|(}

Above all else, you should protect your head.
The ideal helmet will not just protect the top of your head but also your neck and face.

The best helmets are those designed for rescue services and firefighters (\autoref{fig:rescue_helmet}).
These helmets extend further past the ears than a bike helmet.
They often have build in visors that can be lowered over the eyes or the entire face.
Many models have clips for flashlights, and most have attachments for a plastic or leather extension, sometimes called a ``lobster tail'', that extends to cover the neck.
The lobster tail offers a small amount of protection and prevents debris from falling between the wearer's clothing and back.
Rescue helmets usually come with internal adjustable webbing to ensure a good fit both with and without a full-face respirator.
These helmets are the easiest to wear with a respirator since they are specifically designed for this.

\begin{figure}[htb]
\caption{Helmets\supercite{snailsnail}}
\centering
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{rescue-helmet}
	\caption{Rescue}
    \label{fig:rescue_helmet}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=3.2cm, keepaspectratio]{bmx-helmet}
	\caption{BMX}
    \label{fig:bmx_helmet}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=3.2cm, keepaspectratio]{snowboard-helmet}
	\caption{Snowboard}
    \label{fig:snowboard_helmet}
\end{subfigure}
\end{figure}

Rescue helmets are also the most expensive, but as fire departments or EMS providers replace theirs, you may be able to acquire them for cheap or for free.
Despite the cost, if the kinds of actions you attend frequently have violent clashes between police, the added protection is worth it.

Other common options are  BMX-style bicycle helmets (\autoref{fig:bmx_helmet}) and ski or snowboard helmets (\autoref{fig:snowboard_helmet}).
These types are relatively cheap and can easily fit both full-face and half-mask respirators.
Additionally, some medics use tactical, climbing, hockey, and construction helmets, though these are less common.
Whatever kind of helmet you get, ensure it has enough padding to fit snugly on your head both with and without a respirator.

American football or lacrosse helmets are not recommend as the protective metal wiring in front of the jaw area can be easily grabbed, and it is impossible to fit a respirator under them.
Most motorsport helmets are not recommended as they are far too large and heavy for most situations medics encounter.
Baseball helmets are not recommended because they do not have a chin strap and can be easily knocked off the wearer's head.

If wearing a large, protective helmet is infeasible, hardware stores often carry baseball hats with a hard plastic shell on the inside.
These hats look nearly indistinguishable from a normal baseball hat and may help you avoid scrutiny.

If you choose to equip yourself with a helmet, you may want to  mark it with your region's medic logos or symbols.
This makes it easy for others to find in a crowd if they are searching for medical assistance.
Medics may also position themselves as neutral to avoid police repression\index{repression}, and clear markings may help with that.
Further discussion about neutrality can be found in \fullref{ch:general_tactics}.

\index{helmets|)}

\section*{Gloves}
\index{gloves!personal protective equipment|(}

Aside from examination gloves, you may want gloves to keep your hands warm and dry in cold weather or protective gloves.
Mild hypothermia and cold hands lead to reduced dexterity and sensation, both of which can make it hard to render care and to do so safely.

For most occasions, you will want to carry some kind of thick glove to protect you hands from injury.
Common risk to you hands includes shattered glass, projectiles, and riot control measures such as flashbang grenades or rubber bullets.
You will also want gloves that allow you climb and move large objects to reach patients.
You may also have to pick up and throw still burning cans of tear gas, so to avoid thermal burns, your gloves should be thick.
Be cautious of synthetic materials that melt as this will not offer protection from burning tear gas cannisters.

Rescue service gloves (\autoref{fig:rescue_gloves}) are specifically made for exactly these purposes, and they are not very expensive.
They come in a variety of models that usually have thick but soft padding and are resistant to cuts and heat.
Some types additionally have plastic spines to protect the back of the wearer's hands.
Rescue services gloves are recommended.
These gloves also typically do not look like the kind of gloves rioters wear to fight their opposition or cops, so they will draw less scrutiny from law enforcement.

\begin{figure}[htb]
\caption{Gloves\supercite{snailsnail}}
\centering
\begin{subfigure}[b]{0.29\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{rescue-gloves}
    \caption{Rescue}
    \label{fig:rescue_gloves}
\end{subfigure}
\begin{subfigure}[b]{0.35\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{work-gloves}
    \caption{Work}
    \label{fig:work_gloves}
\end{subfigure}
\begin{subfigure}[b]{0.26\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{assault-gloves}
    \caption{Assault}
    \label{fig:assault_gloves}
\end{subfigure}
\end{figure}

The cheapest option for this are simple work gloves (\autoref{fig:work_gloves}), usually made of leather.
They are thick enough to allow you to work with your hands and pick up tear gas canisters, but they do not offer much impact protection.
Some mechanic and work gloves have a plastic spine on the fingers and/or knuckles to protect from impact.
Similarly, assault gloves (\autoref{fig:assault_gloves}), also known as firearms gloves or tactical gloves, will have thicker padding and armor than mechanic gloves.
The drawback is that assault gloves tend to be the most conspicuous which may draw unwanted attention from law enforcement.

\index{gloves!personal protective equipment|)}

\section*{Eye Protection}

You will want to protect your eyes from impact, projectiles, explosives, and chemical agents.
There are many ways to protect your eyes from the various ways they could become injured during an action, and you may choose to acquire one or many options.
Not matter what you pick, you should ensure that the eye protection is shatter proof.

As noted in the discussion of helmets, some helmets come with built or attachable visors.
These will offer protection against projectiles and explosives and may offer some protection against sprayed chemical agents, but not vaporized or aerosolized chemical agents.

The simplest and cheapest solution for protection against projectiles and sprayed chemicals are safety glasses or safety goggles used for construction (\autoref{fig:safety_glasses}).
They are often form-fitting to the face and eye socket and will generally protect against impact, projectiles, sprayed chemical agents.
Safety googles will usually fit around most corrective glasses.
Motocross goggles are more expensive but may be more comfortable to wear with a helmet (\autoref{fig:moto_goggles}).

% TODO should include swim goggles
\begin{figure}[htb]
\caption{Eye Protection\supercite{snailsnail}}
\centering
\begin{subfigure}[b]{4.5cm}
    \centering
    \includesvg[width=4cm, height=4cm, keepaspectratio]{safety-glasses}
    \caption{Safety Glasses}
    \label{fig:safety_glasses}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
    \includesvg[width=4cm, height=4cm, keepaspectratio]{moto-goggles}
    \caption{Motorsport Goggles}
    \label{fig:moto_goggles}
\end{subfigure}
\end{figure}

If you expect tear gas or other vaporized or aerosolized chemical agents to be deployed and do not have a full-face respirator, you can protect your eyes using swimming goggles.
Diving masks should be avoided because respirators are designed to fit over the nose.
Additionally, if you are using ski or motorsport goggles, you will need to seal the vents with caulk or another sealant to help prevent tear gas from entering and irritating the eyes.
You may additionally need to remove the foam padding and replace it with silicone so that it can form a more airtight seal against your face.

All of these options can be combined with half-mask respirators.
Additionally, some medics may wish to double up swimming goggles with motosport goggles for protecting against both chemical agents and impacts.
Regardless of what eye protection you use, be advised that even lightly tinted lenses can be a significant hindrance at night.
The identity protection they provide may not be worth it, or you may want to carry both clear and tinted eye protection.

\section*{Hearing Protection}

There will be many things that can damage your ears and hearing during actions.
Explosive pyrotechnics, flashbang grenades, other explosives, acoustic weapons, and even prolonged exposure to loud music or megaphones can damage your hearing.
The simplest solution to this is to carry disposable foam earplugs.
When acquiring earplugs, look for the highest decibel reduction rating you can find.
You should use a \acrlong{NRR} (\acrshort{NRR}) of -30dB or higher and avoid using a rating lower than -20dB.
Additionally note that some types of rescue helmet have built-in earmuffs, or ear muffs can be acquired and mounted to most types of helmets.

Protesters and other attendees at most actions do not carry hearing protection, so it is useful to carry bulk foam earplugs to hand out.
This is especially true at actions where children and the elderly are present as they tend to be the ones most likely to want hearing protection.
A single earplug can be cut in half lengthwise to fit a child's ear.

\section*{Respirators}
\index{gas masks|see {respirators}}
\index{respirators|(}

Respirators offer protection for your respiratory system from particulate matter and chemical agents.
In particular, you need to protect yourself from riot control agents (\acrshort{RCA}s).
Respirators come in several styles as well as different levels of filtration for different kinds of particles and chemicals.
The standards and specifications vary by region, so for your own safety you may need to do your own research to find a respirator with appropriate filtration levels.
Most major manufacturers rate their products for use in the United States or EU, so these standards will likely apply regardless of where you live.

\begin{figure}[htbp]
\caption{Types of Respirators\supercite{anon-1}}
\label{fig:respirators}
\centering
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{full-face-respirator}
	\caption{Full-Face}
\end{subfigure}
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{half-mask-respirator}
	\caption{Half-Mask}
\end{subfigure}
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{filtering-half-mask}
	\caption{Filtering Half-Mask}
\end{subfigure}
\end{figure}

The kinds of masks that will be discussed are full-face respirators, half-mask respirators, and filtering half-mask respirators.
Examples of these can be found in \autoref{fig:respirators}.
Discussion of other types of respirators is outside the scope of this book since they are not necessary for medics.

\subsection*{Respirator Ratings}

Respirators are rated on how much substance they can filter (protection factor, \acrshort{PF}), how much substance enters gaps in the mask (leakage, fit factor), and what types of substances a filter is capable of removing from the air.

\acrshort{NIOSH} defines three letter markers and three levels for particulate filtration (\autoref{tab:niosh_ratings_particulate}).
Similar ratings in the EU are provided by standard EN 143 for particulate filters (\autoref{tab:en_143}) and EN 149 for filtering half-mask respirators (\autoref{tab:en_149}).

% TODO check does NIOSH ratings account for both filtration and leakage?

\begin{table}[htbp]
\caption{NIOSH Ratings for Particulate Filtration\supercite{niosh-approval-labels}}
\label{tab:niosh_ratings_particulate}
\centering
\begin{tabular}[c]{|l|l|l|}
	\hline
    \multicolumn{1}{|c|}{\textbf{Oil Resistance}} &
		\multicolumn{1}{|c|}{\textbf{Rating}} &
		\multicolumn{1}{|c|}{\textbf{Min. \% particles filtered}} \\
	\hline
	\multirow{3}{*}{Not oil resistant} & N95  & 95\%    \\
	                                   & N99  & 99\%    \\
	                                   & N100 & 99.97\% \\
	\hline
	\multirow{3}{*}{Oil resistant}     & R95  & 95\%    \\
	                                   & R99  & 99\%    \\
	                                   & R100 & 99.97\% \\
	\hline
	\multirow{3}{*}{Oil proof}         & P95  & 95\%    \\
	                                   & P99  & 99\%    \\
	                                   & P100 & 99.97\% \\
	\hline
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{EN 143 Particulate Filtration Ratings}
\label{tab:en_143}
\centering
\begin{tabular}[c]{|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Class}} &
        \multicolumn{1}{|c|}{\textbf{Filter penetration (at 95 L/min air flow)}} \\
    \hline
    P1 & Filters at least 80\% of airborne particles    \\
    P2 & Filters at least 94\% of airborne particles    \\
    P3 & Filters at least 99.95\% of airborne particles \\
    \hline
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{EN 149 Filtering Half-Mask Ratings}
\label{tab:en_149}
\centering
\begin{tabular}[c]{|l|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Class}} &
        \multicolumn{1}{|c|}{\textbf{Filter penetration (at 95 L/min air flow) }} &
        \multicolumn{1}{|c|}{\textbf{Leakage}} \\
    \hline
    FFP1 & Filters at least 80\% of airborne particles    & $< 22\%$ \\
    FFP2 & Filters at least 94\% of airborne particles    & $< 8\%$  \\
    FFP3 & Filters at least 99.95\% of airborne particles & $< 2\%$  \\
    \hline
\end{tabular}
\end{table}

There are two primary modes of failure for filters.
Blockage is when the filters accumulate so much particulate matter that it becomes difficult to breathe.
Breakthrough is when the filter fails and the wearer begins to detect the chemical agent.
Breakthrough may occur immediately if an incorrect filter is selected.
If a proper filter is selected, both of these cases mean you will need to replace the filters.

% TODO this should have the general knowledge of filters (R95+, what kind of color rating, ABEK)

% TODO some claim that activated coal protects against cyanide from expired CS gas, needs research
No matter what kind of respirator or filters you are considering, activated coal filters are not necessary.
They are typically more expensive and do not offer enough of an improvement over similar filters against RCAs to be worth the extra cost and weight.

\subsection*{Respirator Types}

Filtering half-mask respirators are made from a filtering material, cover the mouth and nose, and are held in place with elastic straps.
Some models come with a one-way valve for exhalation.
Simple masks for dust are not sufficient for protection against RCAs.
Masks rated FFP1, FFP2, N59, N99, and N100 should be avoided (though they are better than nothing).
A rating of FFP3 or at least R95 should be used, and respirators rated P100 are ideal.
These masks are typically labeled as being safe to use with paints or solvents.
Note that filtering half-mask respirators are single-use and should be discarded after they are used.

Reusable half-mask respirators are usually made of silicone, cover the mouth and nose, and are held in place with elastic or rubber straps.
These masks may be single use and come with built-in filters, or the filters may be replaceable.
Some models come with no filters, so they must be acquired separately.
The mask's silicone material will fit better against your face than then textile material of a filtering half-mask.
This reduces leakage.

Full-face respirators off the added protection of protecting your eyes from chemical agents.
However, they are larger and more costly than combining swimming goggles with a half-mask.\footnotemark[1]
Like with half-masks, you may need to acquire filters separately.
If you are using the EU standards in \autoref{tab:en_136_classes}, a Class 1 mask is acceptable, Class 2 is preferred\footnotemark[2], and Class 3 is unnecessary.
To protect the plastic lens (face shield) from scratches, you may want to acquire protective covers that can be replaced when they are scratched.
% TODO Note about large lenses vs. smaller lenses

\footnotetext[1]{
Cheap and easy to acquire gas masks are the Russian GP-5 and various types of Israeli civilian gas masks.
The GP-5 has the additional advantage of using standardized \SI{40}{\mm} filters so that replacements can be easily acquired.
The GP-5 has the disadvantage of a significantly reduced field of vision.
I prefer modern full-face and half-mask respirators as they are more ergonomic and have better fields of vision.
}

\footnotetext[2]{
Here ``preferred'' means that they provide superior protection and comfort.
Due to their cost and the risk of damage or theft by the police, you may chose to get a Class 1 mask.
}

\begin{table}[htb]
\caption{EN 136 Full-Face Respirator Classes}
\label{tab:en_136_classes}
\centering
\begin{tabular}[c]{|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Class}} & \multicolumn{1}{|c|}{\textbf{Description}} \\
    \hline
    Class 1 & Light Duty \\
    Class 2 & General Duty \\
    Class 3 & Heavy Duty (for firefighters) \\
    \hline
\end{tabular}
\end{table}

Full-face respirators should not be worn with any kind of glasses.
The arms of the glasses will not allow for a proper seal to be made.
Most industrial full-face respirators will have attachments for corrective lens that can be mounted to the forehead or nose piece.
Do not wear contact lenses with a full-face respirator.
If chemical agents break through, you will have to remove the mask to remove your contacts.
This will lead to additional contamination as well as inhalation of chemical agents.
Likewise, when using goggles in combination with a half-mask respirator, you should not wear glasses or contact lenses.

\subsection*{Respirator Use and Considerations}

When acquiring a full-face or half-mask respirator, take note that they come in different sizes.
You may need to try multiple models to find one that seals well on your face.

If you have facial hair, it should be shaved or trimmed to stubble.
Presence of facial hair can prevent your respirator from sealing tight against your skin.
This can cause leakage.
As a note, you should not shave with a razor directly before an action as the micro-cuts on your skin will be exposed to chemical agents.

Full-face and half-mask respirators should be cleaned after usage, and stored in a cool, dry place.
The exact care for your mask will be included in its instructional guide.

You should store your respirator in an air-tight plastic bag to prevent the filters from degrading.
The exact lifetime of your filters will vary, and their instructional guides will contain this information.
Using any filters after their expiration can be harmful.

When exposed to tear gas during an action, there are steps you can take to don your respirator to minimize how much tear gas you inhale.
To properly don and clear full-face respirator, use the steps below.
This similarly applies to a half-mask respirator used with goggles.

\begin{enumerate}
    \item Stop breathing.
    \item Remove your glasses or contact lenses (if applicable).
    \item Close your eyes.
    \item Put on your respirator and adjust the straps to get a tight fit.
    \item Cover the exhalation valve (if possible), exhale hard to fully to ``clear'' the mask, then uncover the valve.
    \item Cover the inhalation filters (if possible) and attempt to inhale to create negative pressure to help seat the mask on your face, then uncover the filters.
    \item Begin breathing normally and open your eyes.
\end{enumerate}

If you do not have access to a mask, holding a piece of cloth that is damp with water over your nose and mouth can minimize the amount of chemical agent you inhale.
A further discussion of protection against RCAs and related urban legends can be found in \fullref{ch:rca_contamination}.

\index{respirators|)}

\section*{Soft Knee Pads}

In most circumstances, you will not have access to gurneys, examination tables, or even foldable cots.
Patients will often be sitting or laying on the ground while you treat them.
If you have to immobilize a patient's spine, you may be on your knees for many tens of minutes.
Treating patients while on your knees can be discomforting, distracting, or even painful.
Volleyball and handball knee pads are light, soft, and unobtrusive protection you can wear to make you more comfortable.
Alternatively, some work pants have padding sewn into the knees.

\section*{Additional Armoring}

Medics are usually not directly on the front line, and as such in many regions they are not the main target of repression\index{repression} tactics.
Additional protective equipment is generally not necessary.
There are some items that medics will still choose to wear to allow them to do their jobs without fear of injury.

Association football shin guards can be worn and easily hidden under loose pants.
Riot cops may kick the shins of protesters and medics alike when the are trying to get them to move as a way of enacting concealed violence.
Shin guards will make this significantly less painful.

Instead of soft knee pads, medics may wear hard plastic knee pads for skateboarding or inline skating.
If knee pads are worn with shin guards, try to find sets that do not leave an exposed gap between the shin guard and knee pad.
You may want to consider acquiring the combined lower leg pads the have had plastic from the instep of the food to the knee.
Similarly, medics may wear combined elbow and forearm pads.
However, all of these are more conspicuous and may not be necessary unless you are fighting cops on the front line.

Some medics may wear athletic cups to protect their external sex organs against injury from direct or indirect hits from less-lethal projectile weapons including tear gas cannisters and rubber bullets.

Police are trained to target the common peroneal nerve\index{common peroneal nerve} with their batons.
A strike to this nerve can temporarily disable usage of the affected leg (transient neurapraxia)\index{neurapraxia!transient}.
The kind of padded shorts used in American football and downhill mountain biking can offer protection against this kind of temporary disability provided the pads go far enough down the thigh.
These types of pads are meant to be worn under pants and can be somewhat concealed.

If your opponents are armed with knives, even as a medic you may face risk to your life.
A stab proof vest may be advisable, but they may lead to overheating and add weight that would be better suited for medical equipment or other provisions.
Beyond this, small arms fire can be protected against by a bullet proof vest, and protection against higher caliber rounds can be provided by a plate carrier.
These items are large, heavy, and tend to be quite conspicuous.
At the point at which a medic is wearing these, additional protective measures are needed like a kevlar helmet.
Detailed discussion of this level of protection is outside the scope of this book.

\index{personal protective equipment|)}
